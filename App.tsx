import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import { View } from 'react-native';
import AllPlaces from './screens/AllPlaces';
import AddPlace from './screens/AddPlace';
import IconButton from './components/ui/IconButton';
import { createStackNavigator } from '@react-navigation/stack';
import Map from './screens/Map';
import { RootStackParamList } from './types/rootstackparamlist';
import { useState, useEffect, useCallback } from 'react';
import { init } from './utils/database';
import * as SplashScreen from 'expo-splash-screen';
import PlaceDetails from './screens/PlaceDetails';

SplashScreen.preventAutoHideAsync();

const Stack = createStackNavigator<RootStackParamList>();

function StackRoute() {
	const navigation = useNavigation();

	return (
		<Stack.Navigator
			screenOptions={{
				headerTransparent: true,
			}}>
			<Stack.Screen
				name='AllPlaces'
				component={AllPlaces}
				options={{
					title: 'Your favorite places',
					headerRight: () => (
						<IconButton
							icon='add'
							size={24}
							color='black'
							onPress={() =>
								navigation.navigate('AddPlace' as never)
							}
						/>
					),
				}}
			/>
			<Stack.Screen
				name='AddPlace'
				component={AddPlace}
				options={{
					title: 'Add a new place',
				}}
			/>
			<Stack.Screen
				name='Map'
				component={Map}
			/>
			<Stack.Screen
				name='PlaceDetail'
				component={PlaceDetails}
				options={{
					title: 'Loading place...',
				}}
			/>
		</Stack.Navigator>
	);
}

export default function App() {
	const [appIsReady, setAppIsReady] = useState(false);

	useEffect(() => {
		async function prepare() {
			try {
				await init();
				await new Promise((resolve) => setTimeout(resolve, 2000));
			} catch (e) {
				console.log(e);
			} finally {
				setAppIsReady(true);
			}
		}

		prepare();
	}, []);

	const onLayoutRootView = useCallback(async () => {
		if (appIsReady) {
			await SplashScreen.hideAsync();
		}
	}, [appIsReady]);

	if (!appIsReady) {
		return null;
	}

	return (
		<>
			<StatusBar style='auto' />
			<View
				style={{
					flex: 1,
				}}
				onLayout={onLayoutRootView}>
				<NavigationContainer>
					<StackRoute />
				</NavigationContainer>
			</View>
		</>
	);
}
