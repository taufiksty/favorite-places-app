import { PlaceInterface } from '../types/model';

class Place implements PlaceInterface {
	id: string;
	title: string;
	imageUri: string;
	address: string;
	location: {
		lat: number;
		lng: number;
	};

	constructor(
		id: string,
		title: string,
		imageUri: string,
		address: string,
		location: { lat: number; lng: number },
	) {
		this.id = id;
		this.title = title;
		this.imageUri = imageUri;
		this.address = address;
		this.location = location; // {lat: number, lng: number}
	}
}

export default Place;
