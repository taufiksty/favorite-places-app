import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import OutlinedButton from '../components/ui/OutlinedButton';
import colors from '../contants/colors';
import { RouteProp, useNavigation } from '@react-navigation/native';
import { RootStackParamList } from '../types/rootstackparamlist';
import { useEffect, useState } from 'react';
import { fetchPlaceById } from '../utils/database';
import Place from '../models/place';
import { StackNavigationProp } from '@react-navigation/stack';

type PlaceDetailProps = {
	navigation: StackNavigationProp<RootStackParamList, 'PlaceDetail'>;
	route: RouteProp<RootStackParamList, 'PlaceDetail'>;
};

function PlaceDetails({ navigation, route }: PlaceDetailProps) {
	const { placeId } = route.params;
	const [place, setPlace] = useState<Place>();

	useEffect(() => {
		async function load() {
			await fetchPlaceById(placeId).then((p: any) => {
				const placeLoad = new Place(
					p.id,
					p.title,
					p.imageUri,
					p.address,
					{
						lat: p.location.lat,
						lng: p.location.lng,
					},
				);
				setPlace(placeLoad);
				navigation.setOptions({
					title: place?.title,
				});
			});
		}

		load();
	}, [placeId]);

	if (!place) {
		return (
			<View style={styles.fallback}>
				<Text>Loading place...</Text>
			</View>
		);
	}

	function showMapHandler() {
		navigation.navigate('Map', {
			lat: place?.location.lat,
			lng: place?.location.lng,
		});
	}

	return (
		<ScrollView>
			<Image
				style={styles.image}
				source={{ uri: place?.imageUri }}
			/>
			<View style={styles.containerLocation}>
				<View style={styles.containerAddress}>
					<Text style={styles.address}>{place?.address}</Text>
				</View>
				<OutlinedButton
					icon='map'
					onPress={showMapHandler}>
					View on map
				</OutlinedButton>
			</View>
		</ScrollView>
	);
}

export default PlaceDetails;

const styles = StyleSheet.create({
	image: {
		height: '35%',
		minHeight: 300,
		width: '100%',
	},
	fallback: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	containerLocation: {
		alignItems: 'center',
		justifyContent: 'center',
	},
	containerAddress: {
		padding: 20,
	},
	address: {
		color: colors.base[900],
		textAlign: 'center',
		fontWeight: 'bold',
		fontSize: 16,
	},
});
