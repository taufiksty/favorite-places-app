import { useEffect, useState } from 'react';
import List from '../components/places/List';
import colors from '../contants/colors';
import { PlaceInterface } from '../types/model';
import { LinearGradient } from 'expo-linear-gradient';
import { useIsFocused } from '@react-navigation/native';
import { fetchPlaces } from '../utils/database';
import Place from '../models/place';

function AllPlaces() {
	const [places, setPlaces] = useState<PlaceInterface[]>([]);
	const isFocused = useIsFocused();

	useEffect(() => {
		async function load() {
			await fetchPlaces()
				.then((result: any) => {
					const resultPlaces = result.map(
						(p: any) =>
							new Place(p.id, p.title, p.imageUri, p.address, {
								lat: p.location.lat,
								lng: p.location.lng,
							}),
					);
					setPlaces(resultPlaces);
				})
				.catch((e) => console.log(e));
		}

		if (isFocused) {
			load();
		}
	}, []);

	return (
		<LinearGradient
			colors={[colors.aloha[300], colors.aloha[100]]}
			style={{ flex: 1 }}
			start={{ x: 0, y: 0 }}
			end={{ x: 1, y: 1 }}>
			<List places={places} />
		</LinearGradient>
	);
}

export default AllPlaces;
