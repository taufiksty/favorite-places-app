import Form from '../components/places/Form';
import { LinearGradient } from 'expo-linear-gradient';
import colors from '../contants/colors';
import { SafeAreaView, StyleSheet } from 'react-native';
import { PlaceInterface } from '../types/model';
import { insertPlace } from '../utils/database';
import { useNavigation } from '@react-navigation/native';

function AddPlace() {
	const navigation = useNavigation();

	async function onSaveHandler(place: PlaceInterface) {
		await insertPlace(place)
			.then(() => navigation.navigate('AllPlaces' as never))
			.catch((e) => console.log(e));
	}

	return (
		<LinearGradient
			colors={[colors.aloha[300], colors.aloha[100]]}
			style={{ flex: 1 }}
			start={{ x: 0, y: 0 }}
			end={{ x: 1, y: 1 }}>
			<SafeAreaView style={styles.container}>
				<Form onSave={onSaveHandler} />
			</SafeAreaView>
		</LinearGradient>
	);
}

export default AddPlace;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: 64,
		paddingTop: 18,
	},
});
