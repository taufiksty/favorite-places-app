import { RouteProp, useNavigation } from '@react-navigation/native';
import { getCurrentPositionAsync } from 'expo-location';
import { useCallback, useEffect, useLayoutEffect, useState } from 'react';
import { StyleSheet, View, Text, Alert } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import IconButton from '../components/ui/IconButton';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../types/rootstackparamlist';

type LocationPickerProps = {
	latitude: number;
	longitude: number;
};

type MapProps = {
	navigation: StackNavigationProp<RootStackParamList, 'Map'>;
	route: RouteProp<RootStackParamList, 'Map'>;
};

function Map({ navigation, route }: MapProps) {
	const initialLocation = route.params && {
		latitude: route.params.lat as number,
		longitude: route.params.lng as number,
	};

	const [currentLocation, setCurrentLocation] =
		useState<LocationPickerProps | null>(initialLocation);
	const [pickedLocation, setPickedLocation] = useState<LocationPickerProps>();

	useEffect(() => {
		if (initialLocation) {
			return;
		}

		const getCurrentPosition = async () => {
			const currentPosition = await getCurrentPositionAsync();

			setCurrentLocation({
				latitude: currentPosition.coords.latitude,
				longitude: currentPosition.coords.longitude,
			});
		};

		getCurrentPosition();
	}, []);

	const region = {
		latitude: currentLocation?.latitude as number,
		longitude: currentLocation?.longitude as number,
		latitudeDelta: 0.0922,
		longitudeDelta: 0.0421,
	};

	function pickedLocationHandler(event: any) {
		if (initialLocation) {
			return;
		}

		const latitude = event.nativeEvent.coordinate.latitude;
		const longitude = event.nativeEvent.coordinate.longitude;

		setPickedLocation({ latitude, longitude });
	}

	const savePickedLocationHandler = useCallback(() => {
		if (initialLocation) {
			return;
		}

		if (!pickedLocation) {
			Alert.alert(
				'No location picked!',
				'You have to pick a location (by tapping on the map) first!',
			);
			return;
		}

		navigation.navigate('AddPlace', { pickedLocation });
	}, [navigation, pickedLocation]);

	useLayoutEffect(() => {
		if (initialLocation) {
			return;
		}

		navigation.setOptions({
			headerRight: () => (
				<IconButton
					icon='save'
					size={24}
					color='black'
					onPress={savePickedLocationHandler}
				/>
			),
		});
	}, [navigation, savePickedLocationHandler, initialLocation]);

	return (
		<View style={styles.container}>
			{currentLocation ? (
				<MapView
					style={styles.map}
					initialRegion={region}
					onPress={pickedLocationHandler}>
					{pickedLocation ? (
						<Marker
							title='Picked location'
							coordinate={{
								latitude: pickedLocation.latitude,
								longitude: pickedLocation.longitude,
							}}
						/>
					) : (
						<Marker
							title='Location'
							coordinate={{
								latitude: currentLocation.latitude,
								longitude: currentLocation.longitude,
							}}
						/>
					)}
				</MapView>
			) : (
				<Text>Getting your current location...</Text>
			)}
		</View>
	);
}

export default Map;

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	map: {
		width: '100%',
		height: '100%',
	},
});
