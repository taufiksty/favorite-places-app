export type RootStackParamList = {
	AddPlace: {
		pickedLocation?: { latitude: number; longitude: number };
	};
	AllPlaces: {};
	Map: {
		lat?: number;
		lng?: number;
	};
	PlaceDetail: {
		placeId: string;
	};
};
