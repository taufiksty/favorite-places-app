import * as SQLite from 'expo-sqlite';
import { PlaceInterface } from '../types/model';
import Place from '../models/place';

const database = SQLite.openDatabase('places.db');

export async function init() {
	const promise = new Promise(async (resolve, reject) => {
		database.transactionAsync(async (tx) => {
			try {
				const result = await tx.executeSqlAsync(
					`CREATE TABLE IF NOT EXISTS places (
					id INTEGER PRIMARY KEY NOT NULL,
					title TEXT NOT NULL,
					imageUri TEXT NOT NULL,
					address TEXT NOT NULL,
					lat REAL NOT NULL,
					lng REAL NOT NULL)`,
					[],
				);
				resolve(result);
			} catch (error) {
				reject(error);
			}
		});
	});

	return promise;
}

export async function fetchPlaces() {
	const promise = new Promise(async (resolve, reject) => {
		await database.transactionAsync(async (tx) => {
			try {
				const result = await tx.executeSqlAsync(
					`SELECT * FROM places`,
					[],
				);
				resolve(result.rows);
			} catch (error) {
				reject(error);
			}
		});
	});

	return promise;
}

export async function fetchPlaceById(id: string) {
	const promise = new Promise(async (resolve, reject) => {
		await database.transactionAsync(async (tx) => {
			try {
				const result = await tx.executeSqlAsync(
					`SELECT * FROM places WHERE id = ?`,
					[id],
				);
				resolve(result.rows[0]);
			} catch (error) {
				reject(error);
			}
		});
	});

	return promise;
}

export async function insertPlace(place: PlaceInterface) {
	const promise = new Promise(async (resolve, reject) => {
		await database.transactionAsync(async (tx) => {
			console.log('check');
			try {
				const result = await tx.executeSqlAsync(
					`INSERT INTO places (id, title, imageUri, address, lat, lng) VALUES (?, ?, ?, ?, ?, ?)`,
					[
						place.id,
						place.title,
						place.imageUri,
						place.address,
						place.location.lat,
						place.location.lng,
					],
				);
				console.log(result);
				resolve(result);
			} catch (error) {
				reject(error);
			}
		});
	});

	return promise;
}
