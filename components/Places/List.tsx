import { FlatList, View, Text, StyleSheet } from 'react-native';
import { PlaceInterface } from '../../types/model';
import Item from './Item';
import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../types/rootstackparamlist';

interface Props {
	navigation?: StackNavigationProp<RootStackParamList, 'Map'>;
	places: PlaceInterface[];
}

function List({ navigation, places }: Props) {
	if (!places || places.length === 0)
		return (
			<View style={styles.fallbackContainer}>
				<Text style={styles.fallbackText}>
					No places added yet - start adding some!
				</Text>
			</View>
		);

	function onSelectItemHandler(id: string) {
		navigation?.navigate('PlaceDetail', {
			placeId: id,
		});
	}

	return (
		<FlatList
			data={places}
			keyExtractor={(item) => item.id}
			renderItem={({ item }) => (
				<Item
					place={item}
					onSelect={() => onSelectItemHandler(item.id)}
				/>
			)}
			style={styles.container}
		/>
	);
}

export default List;

const styles = StyleSheet.create({
	container: {
		margin: 24,
	},
	fallbackContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	fallbackText: {
		fontSize: 16,
	},
});
