import { Alert, Image, StyleSheet, Text, View } from 'react-native';
import OutlinedButton from '../ui/OutlinedButton';
import colors from '../../contants/colors';
import {
	PermissionStatus,
	getCurrentPositionAsync,
	useForegroundPermissions,
} from 'expo-location';
import { useEffect, useState } from 'react';
import { getAddress, getMapPreview } from '../../utils/location';
import {
	RouteProp,
	useIsFocused,
	useNavigation,
	useRoute,
} from '@react-navigation/native';
import { RootStackParamList } from '../../types/rootstackparamlist';

type LocationPickerProps = {
	latitude: number;
	longitude: number;
};

type LocationProp = {
	latitude: number;
	longitude: number;
	address: string;
};

interface Props {
	onPickedLocation: (location: LocationProp) => void;
}

function LocationPicker({ onPickedLocation }: Props) {
	const [locationPermissionInfo, requestPermission] =
		useForegroundPermissions();
	const [location, setLocation] = useState<LocationPickerProps>();

	const navigation = useNavigation();
	const route = useRoute<RouteProp<RootStackParamList, 'AddPlace'>>();
	const isFocused = useIsFocused();

	useEffect(() => {
		if (isFocused && route?.params) {
			const mapPickedLocation = route.params?.pickedLocation;

			setLocation(mapPickedLocation);
		}
	}, [route, isFocused]);

	useEffect(() => {
		async function handleLocation() {
			if (location) {
				const address = await getAddress(
					location.latitude,
					location.longitude,
				);
				onPickedLocation({ ...location, address });
			}
		}
		handleLocation();
	}, [location, onPickedLocation]);

	async function verifyPermissions(): Promise<boolean> {
		if (
			locationPermissionInfo?.status === PermissionStatus.DENIED ||
			locationPermissionInfo?.status === PermissionStatus.UNDETERMINED
		) {
			const permissionResponse = await requestPermission();
			if (!permissionResponse.granted) {
				Alert.alert(
					'Insufficient permission!',
					'You need to grant camera permission to use this app.',
				);
				return false;
			}
		}

		return true;
	}

	async function getLocationHandler() {
		const permission = await verifyPermissions();

		if (!permission) return;
		const location = await getCurrentPositionAsync();
		setLocation({
			latitude: location.coords.latitude,
			longitude: location.coords.longitude,
		});
	}

	async function pickOnMapHandler() {
		const permission = await verifyPermissions();

		if (!permission) return;

		navigation.navigate('Map' as never);
	}

	let locationPreview = (
		<Text style={styles.textPreview}>No location chosen yet.</Text>
	);

	if (location) {
		locationPreview = (
			<Image
				style={styles.image}
				source={{
					uri: getMapPreview(location.latitude, location.longitude),
				}}
			/>
		);
	}

	return (
		<View>
			<View style={[styles.mapPreview, !location && styles.downOpacity]}>
				{locationPreview}
			</View>
			<View style={styles.btnContainer}>
				<OutlinedButton
					icon='location'
					onPress={getLocationHandler}>
					Locate user
				</OutlinedButton>
				<OutlinedButton
					icon='map'
					onPress={pickOnMapHandler}>
					Pick on map
				</OutlinedButton>
			</View>
		</View>
	);
}

export default LocationPicker;

const styles = StyleSheet.create({
	mapPreview: {
		width: '100%',
		height: 200,
		marginVertical: 8,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 4,
		backgroundColor: colors.interdimensionalBlue[100],
		overflow: 'hidden',
	},
	downOpacity: {
		opacity: 0.5,
	},
	image: {
		width: '100%',
		height: '100%',
	},
	btnContainer: {
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
	},
	textPreview: {
		color: colors.base[900],
	},
});
