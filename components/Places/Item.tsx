import { Image, Pressable, View, Text, StyleSheet } from 'react-native';
import { PlaceInterface } from '../../types/model';
import colors from '../../contants/colors';

interface Props {
	place: PlaceInterface;
	onSelect: () => void;
}

function Item({ place, onSelect }: Props) {
	return (
		<Pressable
			onPress={onSelect}
			style={({ pressed }) => [
				styles.container,
				pressed && styles.pressed,
			]}>
			<Image
				source={{ uri: place.imageUri }}
				style={styles.Image}
			/>
			<View style={styles.containerInfo}>
				<Text style={styles.title}>{place.title}</Text>
				<Text style={styles.address}>{place.address}</Text>
			</View>
		</Pressable>
	);
}

export default Item;

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		alignItems: 'flex-start',
		borderRadius: 6,
		marginVertical: 12,
		backgroundColor: colors.base[200],
		elevation: 2,
		shadowColor: 'black',
		shadowOffset: { width: 1, height: 1 },
		shadowOpacity: 0.25,
		shadowRadius: 2,
	},
	pressed: {
		opacity: 0.5,
	},
	Image: {
		flex: 1,
		borderBottomLeftRadius: 6,
		borderTopLeftRadius: 6,
		height: 100,
	},
	containerInfo: {
		flex: 2,
		padding: 2,
	},
	title: {
		fontWeight: 'bold',
		fontSize: 18,
		color: colors.base[900],
	},
	address: {
		fontSize: 18,
		color: colors.base[900],
	},
});
