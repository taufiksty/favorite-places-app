import { useCallback, useState } from 'react';
import { ScrollView, View, Text, TextInput, StyleSheet } from 'react-native';
import ImagePicker from './ImagePicker';
import colors from '../../contants/colors';
import LocationPicker from './LocationPicker';
import Button from '../ui/Button';
import { PlaceInterface } from '../../types/model';
import Place from '../../models/place';

interface Props {
	onSave: (place: PlaceInterface) => void;
}

type LocationProp = {
	latitude: number;
	longitude: number;
	address: string;
};

type InputProps = {
	title: string;
	imageUri: string;
	location: LocationProp;
};

function Form({ onSave }: Props) {
	const [input, setInput] = useState<InputProps>({
		title: '',
		imageUri: '',
		location: { latitude: 0, longitude: 0, address: '' },
	});

	function onTakenImageHandler(imageUri: string) {
		setInput({ ...input, imageUri });
	}

	const onPickedLocationHandler = useCallback((location: LocationProp) => {
		setInput({ ...input, location });
	}, []);

	function submitPlaceHandler() {
		const id = new Date().toDateString() + Math.random().toString();
		const place = new Place(
			id,
			input.title,
			input.imageUri,
			input.location.address,
			{ lat: input.location.latitude, lng: input.location.longitude },
		);

		onSave(place);
	}

	return (
		<ScrollView style={styles.container}>
			<View>
				<Text style={styles.label}>Title</Text>
				<TextInput
					value={input?.title}
					onChangeText={(value) =>
						setInput({ ...input, title: value })
					}
					style={styles.input}
				/>
			</View>
			<ImagePicker onTakenImage={onTakenImageHandler} />
			<LocationPicker onPickedLocation={onPickedLocationHandler} />
			<Button onPress={submitPlaceHandler}>Add place</Button>
		</ScrollView>
	);
}

export default Form;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 24,
	},
	label: {
		fontWeight: 'bold',
		marginBottom: 4,
		color: colors.base[800],
	},
	input: {
		paddingVertical: 4,
		fontSize: 16,
		borderBottomWidth: 1,
		borderBottomColor: colors.base[800],
	},
});
