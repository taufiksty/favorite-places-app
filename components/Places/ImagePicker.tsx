import {
	PermissionStatus,
	launchCameraAsync,
	useCameraPermissions,
} from 'expo-image-picker';
import { useState } from 'react';
import { Alert, Image, StyleSheet, View, Text } from 'react-native';
import colors from '../../contants/colors';
import OutlinedButton from '../ui/OutlinedButton';

interface Props {
	onTakenImage: (imageUri: string) => void;
}

function ImagePicker({ onTakenImage }: Props) {
	const [image, setImage] = useState<string | null>();
	const [cameraPermissionInfo, requestPermission] = useCameraPermissions();

	async function verifyPermissions(): Promise<boolean> {
		if (
			cameraPermissionInfo?.status === PermissionStatus.DENIED ||
			cameraPermissionInfo?.status === PermissionStatus.UNDETERMINED
		) {
			const permissionResponse = await requestPermission();
			if (!permissionResponse.granted) {
				Alert.alert(
					'Insufficient permission!',
					'You need to grant camera permission to use this app.',
				);
				return false;
			}
		}

		return true;
	}

	async function takeImageHandler() {
		const permission = await verifyPermissions();

		if (!permission) {
			return;
		}

		const image = await launchCameraAsync({
			allowsEditing: true,
			aspect: [16, 9],
			quality: 0.5,
		});

		setImage(image.assets && (image.assets[0].uri as string));
		onTakenImage(image.assets ? image.assets[0].uri : '');
	}

	let imagePreview = (
		<Text style={styles.textPreview}>No image taken yet.</Text>
	);

	if (image) {
		imagePreview = (
			<Image
				source={{ uri: image }}
				style={styles.image}
			/>
		);
	}

	return (
		<View style={styles.container}>
			<View
				style={
					image
						? [styles.imagePreviewContainer]
						: [styles.imagePreviewContainer, styles.downOpacity]
				}>
				{imagePreview}
			</View>
			<OutlinedButton
				onPress={takeImageHandler}
				icon='camera'>
				Take image
			</OutlinedButton>
		</View>
	);
}

export default ImagePicker;

const styles = StyleSheet.create({
	container: {
		marginVertical: 16,
	},
	imagePreviewContainer: {
		width: '100%',
		height: 200,
		marginVertical: 16,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 4,
		backgroundColor: colors.interdimensionalBlue[100],
	},
	downOpacity: {
		opacity: 0.5,
	},
	image: {
		width: '100%',
		height: '100%',
	},
	textPreview: {
		color: colors.base[900],
	},
});
