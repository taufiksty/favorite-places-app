import { Pressable, StyleSheet, View, ViewStyle } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

interface Props {
	icon: keyof (typeof Ionicons)['glyphMap'];
	size: number;
	color: string;
	style?: ViewStyle;
	onPress: () => void;
}

function IconButton({ icon, size, color, style, onPress }: Props) {
	return (
		<Pressable
			onPress={onPress}
			style={({ pressed }) =>
				pressed
					? [styles.button, styles.pressed, style]
					: [styles.button, style]
			}>
			<Ionicons
				name={icon}
				size={size}
				color={color}
			/>
		</Pressable>
	);
}

export default IconButton;

const styles = StyleSheet.create({
	button: {
		margin: 4,
		padding: 8,
		justifyContent: 'center',
		alignItems: 'center',
	},
	pressed: {
		opacity: 0.7,
	},
});
