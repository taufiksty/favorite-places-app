import { Pressable, StyleSheet, Text } from 'react-native';
import colors from '../../contants/colors';

interface Props {
	children: string;
	onPress: () => void;
}

function Button({ children, onPress }: Props) {
	return (
		<Pressable
			style={({ pressed }) => [
				styles.container,
				pressed && styles.pressed,
			]}
			onPress={onPress}>
			<Text style={styles.text}>{children}</Text>
		</Pressable>
	);
}

export default Button;

const styles = StyleSheet.create({
	container: {
		paddingHorizontal: 12,
		paddingVertical: 8,
		marginVertical: 32,
		marginHorizontal: 4,
		backgroundColor: colors.base[900],
		elevation: 2,
		borderRadius: 4,
		shadowColor: 'black',
		shadowOffset: { width: 1, height: 1 },
		shadowOpacity: 0.25,
		shadowRadius: 2,
	},
	pressed: {
		opacity: 0.7,
	},
	text: {
		textAlign: 'center',
		fontSize: 16,
		color: colors.base[100],
	},
});
