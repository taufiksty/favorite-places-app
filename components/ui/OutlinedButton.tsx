import { Pressable, StyleSheet, Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import colors from '../../contants/colors';

interface Props {
	onPress: () => void;
	icon: React.ComponentProps<typeof Ionicons>['name'];
	children: string;
}

function OutlinedButton({ onPress, icon, children }: Props) {
	return (
		<Pressable
			onPress={onPress}
			style={({ pressed }) => [
				styles.buttonContainer,
				pressed && styles.pressed,
			]}>
			<Ionicons
				name={icon}
				size={18}
				color={colors.base[800]}
				style={styles.icon}
			/>
			<Text style={styles.text}>{children}</Text>
		</Pressable>
	);
}

export default OutlinedButton;

const styles = StyleSheet.create({
	buttonContainer: {
		margin: 4,
		paddingHorizontal: 12,
		paddingVertical: 6,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		borderWidth: 1,
		borderRadius: 4,
		borderColor: colors.base[800],
	},
	pressed: {
		opacity: 0.7,
	},
	icon: {
		marginRight: 6,
	},
	text: {
		color: colors.base[800],
	},
});
